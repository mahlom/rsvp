﻿<?php

define('EMAIL_FOR_REPORTS', '');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

define('_DIR_', str_replace('\\', '/', dirname(__FILE__)) . '/');
require_once _DIR_ . '/handler.php';

require_once '/var/www/html/lib/functions.php';
require_once '/var/www/html/lib/mysql.php';
require_once '/var/www/html/lib/brpdf.php';
require_once '/var/www/html/phpMailer/class.phpmailer.php';
require_once '/var/www/html/phpMailer/class.smtp.php';

// ** Event Hosts
define('EVENT_HOSTS', "SCC EVENTS GmbH");

$data = $_POST;

//
$display_frmd_msg = false;

//value to store a successful update/new rsvp
$success = false;

//value to store if the server checks are passed
$passed = false;

$sendmail = false;

//value to store the return message
$message = '';

if ($data['submit']) {
	
	$conn = getConn();
	
	if ($conn->connect_error) {
		die ('Error connecting to mysql: '.$conn->connect_errno.' '.$conn->connect_error);
	}
	
	$conn->set_charset("utf8");
		
	$query = sprintf("INSERT INTO vip_br SET IP = '%s', EMAIL = '%s', ANREDE0 = '%s', VORNAME0 = '%s', NAME0 = '%s', FIRMA0 = '%s', ATTEND = %d, PHONENUMBER = '%s', COMMENTS = '%s', GUESTS = %d, NAME1 = '%s', STREET0 = '%s', PLZ0 = %d, CITY0 = '%s'",
		mysqli_real_escape_string($conn, get_ip()),
		mysqli_real_escape_string($conn, $data['email']),
		mysqli_real_escape_string($conn, $data['anrede0']),
		mysqli_real_escape_string($conn, $data['namefirst']),
		mysqli_real_escape_string($conn, $data['namelast']),
		mysqli_real_escape_string($conn, $data['input1']),
		mysqli_real_escape_string($conn, $data['attendance']),
		mysqli_real_escape_string($conn, $data['phone']),
		mysqli_real_escape_string($conn, $data['comments']),
		mysqli_real_escape_string($conn, $data['guests']),
		mysqli_real_escape_string($conn, $data['input2']),
		mysqli_real_escape_string($conn, $data['addressaddr1']),
		mysqli_real_escape_string($conn, $data['addresszip']),
		mysqli_real_escape_string($conn, $data['addresscity']));
	
	$success = mysqli_query($conn, $query);
	
	if (!$success) {
		die ('Error executing query');
	}
	//send out a confirmation email 

	$lfdNr = mysqli_insert_id($conn);
	
	//daten zusammensuchen
	$anrede = $data['anrede0'];
	$vorname = $data['namefirst'];
	$name = $data['namelast'];
	$guest = $data['guests'];
	$geehrte = "geehrte";
	if ($anrede == "Herr") {
		$geehrte .= "r";
	}

	$from = 'no-reply@scc-events.com'; 	// confirmation from email address
	$msgAttend = "vielen Dank f&uuml;r Ihre R&uuml;ckmeldung.<br/>Im Anhang finden Sie ihre Anmeldebest&auml;tigung. Bitte bringen Sie diese ausgedruckt oder digital auf Ihrem Smartphone mit. Sie ist Ihre Eintrittskarte!<br/>Wir freuen uns auf Ihr Erscheinen!<br/><br/>Sportliche Gr&uuml;&szlig;e,<br/>Ihre SCC EVENTS GmbH";
	
	$mail = getMailObj();
	$mail->setFrom('no-reply@scc-events.com', 'SCC EVENTS GmbH');
	$mail->addAddress($data['email']);
	$mail->Subject = "Bestätigung Rückmeldung VIP-Brunch 37. Berliner Halbmarathon";
	if ($data['attendance']=='1') {
		$mailMessage = "Sehr {$geehrte} {$anrede} {$name},<br/><br/>{$msgAttend}";
		$message = "Danke {$anrede} {$name}, wir freuen uns, dass Sie teilnehmen werden. Wir haben eine Email mit Ihrer Best&auml;tigung an folgende Adresse geschickt: ".$data['email'].". Bitte &uuml;berpr&uuml;fen Sie ggf. Ihren Spam Ordner.";
		// PDF erzeugen
		$pdfde = new BrunchPDFde();
		$pdfde->AliasNbPages();
		$pdfde->AddPage();
		$pdfde->initText('V-BR-'.$lfdNr, $vorname, $name, $guest);
		$filenameDe = '/var/www/html/tmp/V-BR-'.$lfdNr.'-Bestaetigung_'.$name.'.pdf';
		$attachmentDe = $pdfde->Output($filenameDe, 'F');

		$mail->addAttachment($filenameDe, "Bestaetigung.pdf");
		
		$mail->addAttachment('/var/www/html/tmp/VIP-Brunch.pdf', "Plan.pdf");
		
		$sendmail = true;

	} else {
		$message = "Danke {$anrede} {$name}, schade, dass Sie nicht teilnehmen k&ouml;nnen.";
		$display_frmd_msg = true;
	} 
	
	if ($sendmail) {
		$mail->isHTML(true);
		$mail->Body = $mailMessage;
		
		if (!$mail->send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			$display_frmd_msg = true;
		}
	} 
	mysqli_close($conn);
}
?>

<?php if ($display_frmd_msg): ?>
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-solid-green.css" type="text/css" />
<form class="formoid-solid-green" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:800px;min-width:150px" method="post" action="">
	<div class="title"><h2><center>R&uuml;ckmeldung erfolgreich!</center></h2></div><div style="margin: 8px;"><?php echo $message; ?><br/><p>Zur Veranstaltungswebsite des <a href="http://www.berliner-halbmarathon.de">Berliner Halbmarathon</a></p><br/></div>
</form>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-solid-green.css" type="text/css" />
<script type="text/javascript" src="<?php echo dirname($form_path); ?>/jquery.min.js"></script>

<form class="formoid-solid-green" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:800px;min-width:150px" method="post" action="">
	<div class="title"><h2><center>Anmeldung zum VIP&#8209;Brunch am 2.&nbsp;April&nbsp;2017</center></h2><div style="margin: 8px;">Wenn Sie eine VIP&#8209;Einladung von uns erhalten haben, k&ouml;nnen Sie sich auf dieser Seite f&uuml;r den VIP&#8209;Brunch im Rahmen des 37.&nbsp;Berliner&nbsp;Halbmarathon anmelden.</br>Wenn Sie teilnehmen, bekommen Sie im Anschluss eine Email mit einem PDF Dokument. Bitte bringen Sie dieses PDF ausgedruckt mit, es ist Ihre Eintrittskarte!</div></div>
	<div class="element-email<?php frmd_add_class("email"); ?>"><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="email" name="email" value="" required="required" placeholder="Email"/><span class="icon-place"></span></div></div>
	<div class="element-select"><label class="title"><span class="required">*</span></label><div class="item-cont"><div class="small"><span><select name="anrede0" required="required">
		<option value="" disabled selected hidden>Anrede</option>
		<option value="Herr">Herr</option>
		<option value="Frau">Frau</option></select><i></i><span class="icon-place"></span></span></div></div></div>	
	<div class="element-name<?php frmd_add_class("name"); ?>"><label class="title"><span class="required">*</span></label><span class="nameFirst"><input placeholder="Vorname" type="text" size="8" name="namefirst" required="required"/><span class="icon-place"></span></span><span class="nameLast"><input placeholder="Nachname" type="text" size="14" name="namelast" required="required"/><span class="icon-place"></span></span></div>
	<div class="element-input<?php frmd_add_class("input1"); ?>"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input1" placeholder="Firma"/><span class="icon-place"></span></div></div>
	<div class="element-radio<?php frmd_add_class("radio"); ?>"><label class="title">Teilnahme<span class="required">*</span></label><div class="column column1"><label><input type="radio" name="attendance" value="1" required="required"/><span>Ja, ich werde teilnehmen</span></label><label><input type="radio" name="attendance" value="0" required="required"/><span>Nein, ich werde nicht teilnehmen</span></label></div><span class="clearfix"></span></div>
	<div class="element-select<?php frmd_add_class("select"); ?>"><label class="title"><span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="guests" required="required">
		<option value="" disabled selected hidden>Bitte wählen...</option>
		<option value="0">ohne Begleitung</option>
		<option value="1">eine Begleitperson</option></select><i></i><span class="icon-place"></span></span></div></div></div>
	<div class="element-input<?php frmd_add_class("input2"); ?>"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input2" placeholder="Name der Begleitung"/><span class="icon-place"></span></div></div>
	<div class="element-textarea<?php frmd_add_class("textarea"); ?>"><label class="title"></label><div class="item-cont"><textarea class="medium" name="comments" cols="20" rows="5" placeholder="Kommentar / Hinweis"></textarea><span class="icon-place"></span></div></div>
<div class="submit"><input name="submit" type="submit" value="Absenden"/></div></form><script type="text/javascript" src="<?php echo dirname($form_path); ?>/formoid-solid-green.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>
<?php frmd_end_form(); ?>