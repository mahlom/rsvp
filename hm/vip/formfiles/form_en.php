﻿<?php

define('EMAIL_FOR_REPORTS', '');
define('RECAPTCHA_PRIVATE_KEY', '@privatekey@');
define('FINISH_URI', 'http://');
define('FINISH_ACTION', 'message');
define('FINISH_MESSAGE', 'Thanks for filling out my form!');
define('UPLOAD_ALLOWED_FILE_TYPES', 'doc, docx, xls, csv, txt, rtf, html, zip, jpg, jpeg, png, gif');

define('_DIR_', str_replace('\\', '/', dirname(__FILE__)) . '/');
require_once _DIR_ . '/handler.php';

require_once '/var/www/html/lib/functions.php';
require_once '/var/www/html/lib/mysql.php';
require_once '/var/www/html/lib/brpdf.php';
require_once '/var/www/html/phpMailer/class.phpmailer.php';
require_once '/var/www/html/phpMailer/class.smtp.php';

// ** Event Hosts
define('EVENT_HOSTS', "SCC EVENTS GmbH");

$data = $_POST;

//
$display_frmd_msg = false;

//value to store a successful update/new rsvp
$success = false;

//value to store if the server checks are passed
$passed = false;

$sendmail = true;

//value to store the return message
$message = '';

if ($data['submit']) {
	
	$conn = getConn();
	
	if ($conn->connect_error) {
		die ('Error connecting to mysql: '.$conn->connect_errno.' '.$conn->connect_error);
	}
	
	$conn->set_charset("utf8");
		
	
		//submit the data
	$query = sprintf("INSERT INTO vip_br SET IP = '%s', EMAIL = '%s', ANREDE0 = '%s', VORNAME0 = '%s', NAME0 = '%s', FIRMA0 = '%s', ATTEND = %d, PHONENUMBER = '%s', COMMENTS = '%s', GUESTS = %d, NAME1 = '%s', STREET0 = '%s', PLZ0 = %d, CITY0 = '%s'",
		mysqli_real_escape_string($conn, get_ip()),
		mysqli_real_escape_string($conn, $data['email']),
		mysqli_real_escape_string($conn, $data['anrede0']),
		mysqli_real_escape_string($conn, $data['namefirst']),
		mysqli_real_escape_string($conn, $data['namelast']),
		mysqli_real_escape_string($conn, $data['input1']),
		mysqli_real_escape_string($conn, $data['attendance']),
		mysqli_real_escape_string($conn, $data['phone']),
		mysqli_real_escape_string($conn, $data['comments']),
		mysqli_real_escape_string($conn, $data['guests']),
		mysqli_real_escape_string($conn, $data['input2']),
		mysqli_real_escape_string($conn, $data['addressaddr1']),
		mysqli_real_escape_string($conn, $data['addresszip']),
		mysqli_real_escape_string($conn, $data['addresscity']));
			
	$success = mysqli_query($conn, $query);
	
	if (!$success) {
		die ('Error executing query');
	}
	//send out a confirmation email 

	$lfdNr = mysqli_insert_id($conn);
	
	//daten zusammensuchen
	$vorname = $data['namefirst'];
	$name = $data['namelast'];
	$guest = $data['guests'];

	$from = 'no-reply@scc-events.com'; 	// confirmation from email address
	$msgAttend = "thank you for the RSVP.<br/>Your confirmation PDF is attached to this email. Please bring a printed copy along, it is your entry ticket!<br/>We are looking forward to seeing you there.<br/><br/>Best regards,<br/>Your SCC EVENTS GmbH";
	
	$mail = getMailObj();
	$mail->setFrom('no-reply@scc-events.com', 'SCC EVENTS GmbH');
	$mail->addAddress($data['email']);
	$mail->Subject = "RSVP for the VIP-Brunch of the Berlin Half Marathon 2017";
	if ($data['attendance']=='1') {
		$mailMessage = "Dear {$geehrte} {$vorname} {$name},<br/><br/>{$msgAttend}";
		$message = "Thank you {$vorname} {$name}, we are looking forward to seeing you there.<br/>An email with your confirmation has been sent to: ".$data['email'].". Please also check your spam folder.";
		// PDF erzeugen
		
		$pdfen = new BrunchPDFen();
		$pdfen->AliasNbPages();
		$pdfen->AddPage();
		$pdfen->initText('V-BR-'.$lfdNr, $vorname, $name, $guest);
		$filenameEn = '/var/www/html/tmp/V-BR-'.$lfdNr.'-Confirmation_'.$name.'.pdf';
		$attachmentEn = $pdfen->Output($filenameEn, 'F');

		$mail->addAttachment($filenameEn, "Confirmation.pdf");
		
//		$mail->addAttachment('/var/www/html/tmp/VIP-Brunch.pdf', "Map.pdf");
		$sendmail = true;

	} else {
		$message = "Thank you {$vorname} {$name}, it is a pity that you can not attend.";
		$display_frmd_msg = true;
	} 
	
	if ($sendmail) {
		$mail->isHTML(true);
		$mail->Body = $mailMessage;

		if (!$mail->send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			$display_frmd_msg = true;
		}
	}
	
	mysqli_close($conn);
}
?>

<?php if ($display_frmd_msg): ?>
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-solid-green.css" type="text/css" />
<form class="formoid-solid-green" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:800px;min-width:150px" method="post" action="">
	<div class="title"><h2><center>RSVP successfull!</center></h2></div><div style="margin: 8px;"><?php echo $message; ?><br/><p>Visit the webpage of the <a href="http://www.berliner-halbmarathon/en">Berlin Half Marathon</a></p><br/></div>
</form>
<?php else: ?>
<!-- Start Formoid form-->
<link rel="stylesheet" href="<?php echo dirname($form_path); ?>/formoid-solid-green.css" type="text/css" />
<script type="text/javascript" src="<?php echo dirname($form_path); ?>/jquery.min.js"></script>

<form class="formoid-solid-green" style="background-color:#FFFFFF;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:800px;min-width:150px" method="post" action="">
	<div class="title"><h2><center>RSVP for the VIP&#8209;Brunch on April&nbsp;2,&nbsp;2017</center></h2><div style="margin: 8px;">If you received a VIP invitation from us, you can rsvp on this page for the VIP&#8209;Brunch of the 37th Berlin&nbsp;Half Marathon.</br>If you attend, you will receive an email with a PDF document. Please bring along a printed copy, it is your entry ticket!</div></div>
	<div class="element-email<?php frmd_add_class("email"); ?>"><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="email" name="email" value="" required="required" placeholder="Email"/><span class="icon-place"></span></div></div>
	<div class="element-select"><label class="title"><span class="required">*</span></label><div class="item-cont"><div class="small"><span><select name="anrede0" required="required">
		<option value="" disabled selected hidden>Salutation</option>
		<option value="Mr.">Mr.</option>
		<option value="Mrs.">Mrs.</option>
		<option value="Ms.">Ms.</option></select><i></i><span class="icon-place"></span></span></div></div></div>	
	<div class="element-name<?php frmd_add_class("name"); ?>"><label class="title"><span class="required">*</span></label><span class="nameFirst"><input placeholder="First name" type="text" size="8" name="namefirst" required="required"/><span class="icon-place"></span></span><span class="nameLast"><input placeholder="Last name" type="text" size="14" name="namelast" required="required"/><span class="icon-place"></span></span></div>
	<div class="element-input<?php frmd_add_class("input1"); ?>"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input1" placeholder="Company"/><span class="icon-place"></span></div></div>
	<div class="element-radio<?php frmd_add_class("radio"); ?>"><label class="title">Attendance<span class="required">*</span></label><div class="column column1"><label><input type="radio" name="attendance" value="1" required="required"/><span>Yes, I will attend</span></label><label><input type="radio" name="attendance" value="0" required="required"/><span>No, I will not attend</span></label></div><span class="clearfix"></span></div>
	<div class="element-select<?php frmd_add_class("select"); ?>"><label class="title"><span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="guests" required="required">
		<option value="" disabled selected hidden>Please choose...</option>
		<option value="0">no companion</option>
		<option value="1">one companion</option></select><i></i><span class="icon-place"></span></span></div></div></div>
	<div class="element-input<?php frmd_add_class("input2"); ?>"><label class="title"></label><div class="item-cont"><input class="large" type="text" name="input2" placeholder="Name of companion"/><span class="icon-place"></span></div></div>
	<div class="element-textarea<?php frmd_add_class("textarea"); ?>"><label class="title"></label><div class="item-cont"><textarea class="medium" name="comments" cols="20" rows="5" placeholder="comments"></textarea><span class="icon-place"></span></div></div>
<div class="submit"><input name="submit" type="submit" value="Submit"/></div></form><script type="text/javascript" src="<?php echo dirname($form_path); ?>/formoid-solid-green.js"></script>

<!-- Stop Formoid form-->
<?php endif; ?>
<?php frmd_end_form(); ?>