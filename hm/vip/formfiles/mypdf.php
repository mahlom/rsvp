<?php
require('../fpdf/fpdf.php');

class BrunchPDF extends FPDF
{
	// Page header
	function Header()
	{
		// Logo
		$this->Image('../assets/logo.jpg',145,7,55);
		// Arial bold 15
		$this->SetFont('Arial','B',15);
		// Move to the right
//		$this->Cell(80);
		// Title
//		$this->Cell(30,10,'Title',1,0,'C');
		// Line break
//		$this->Ln(20);
	}

	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom
//S		$this->SetY(-40);
		// Arial italic 8
	//    $this->SetFont('Arial','I',8);
		// Page number
	//    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		// Logoleiste
		$this->Image('../assets/logoleiste.jpg',10,235,190);
		$this->Image('../assets/a_wmm.jpg',55,275,100);
	}
	
	function addAnschrift($anrede='Herr', $name='Maximilian Mustermann', $strasse='Musterstr. 123', $plz_ort='12345 Musterstadt') {
		$this->SetY(60);
		$this->SetFont('Arial','',11);
		$this->Cell(50, 6, $anrede);
		$this->ln();
		$this->Cell(50, 6, $name);
		$this->ln();
		$this->Cell(50, 6, $strasse);
		$this->ln(8);
		$this->Cell(50, 6, $plz_ort);
	}
	
	private function addBetreff($text) {
		$this->SetY(50);
		$this->SetFont('Arial','B',11);
		$this->Cell(0, 6, $text);
	}
	
	function addAnrede($anrede, $name) {
		$start = 'Sehr geehrte';
		if ($anrede=='Herr') {
			$start = $start.'r';
		}
		$text = $start.' '.$anrede.' '.$name.',';
		$this->SetY(65);
		$this->SetFont('Arial','',11);
		$this->Cell(50, 6, $text);
	}

	private function addDanke($guests) {
		if ($guests) {
			$begleitung='mit Begleitung ';
		}
		else {
			$begleitung='';
		}
		$text = 'wir freuen uns �ber Ihre Zusage '.$begleitung.'zu unserem VIP-Brunch am 25.09.2016 anl�sslich des 43. BMW BERLIN-MARATHON.';
		$this->SetY(75);
		$this->Write(5, $text);
		$this->ln(8);
	}
	
	private function addInhalt() {
		$this->SetFont('Arial','BU',12);
		$this->Write(5, 'Bitte bringen Sie diese Anmeldebest�tigung zur Einlasskontrolle mit.');
		$this->ln(10);
		$this->SetFont('Arial','B',11);
		$this->Write(5, 'AB 9.00 Uhr');
		$this->ln();
		$this->Write(5, 'VIP-Lounge im Zielbereich');
		$this->ln();
		$this->Write(5, 'Stra�e des 17. Juni');
		$this->ln();
		$this->Write(5, '(gegen�ber dem Sowjetischen Ehrenmal)');
		$this->ln();
		$this->Write(5, '10785 Berlin');
		$this->ln(10);
		$this->programmAblauf();
		$this->ln(10);
		$this->AnfahrtsHinweise();
		$this->addPage();
		$this->setY(40);
		$this->anfahrtOeffis();
		$this->ln(10);
		$this->zugangViplounge();
		$this->ln(10);
		$this->Image('../assets/anfahrt.png',35,130,140);
	}
	
	private function programmAblauf() {
		$this->SetFont('Arial','B',11);
		$this->Write(5, 'VIP-Brunch  � Programmablauf:');
		$this->ln(6);
		$this->Write(5, 'Sonntag, 25. September 2016');
		$this->ln(6);
		$this->Cell(40, 5, '09:00 Uhr');
//		$this->ln();
		$this->SetFont('Arial','',11);
		$this->Write(5, 'Einlass zum VIP-Brunch - Er�ffnung des Buffets');
		$this->ln();
		$this->SetFont('Arial','B',11);
		$this->Cell(40, 5, '10:30 Uhr');
//		$this->ln();
		$this->SetFont('Arial','',11);
		$this->Write(5, 'Offizielle Begr��ung');
		$this->ln();
		$this->SetFont('Arial','B',11);
		$this->Cell(40, 5, 'ab 11:15 Uhr');
//		$this->ln();
		$this->SetFont('Arial','',11);
		$this->Write(5, 'Zieleinlauf der ersten L�ufer und L�uferinnen');
		$this->ln();
		$this->SetFont('Arial','B',11);
		$this->Cell(40, 5, '12:00 Uhr');
//		$this->ln();
		$this->SetFont('Arial','',11);
		$this->Write(5, 'Siegerehrung');
		$this->ln();
		$this->SetFont('Arial','B',11);
		$this->Cell(40, 5, '15:00 Uhr');
//		$this->ln();
		$this->SetFont('Arial','',11);
		$this->Write(5, 'Veranstaltungsende');
	}
	
	private function anfahrtsHinweise() {
		$this->SetFont('Arial','B',11);
		$this->write(5, 'Anfahrtshinweise');
		$this->ln();
		$this->SetFont('Arial','',11);
		$this->write(5, 'Die Anreise zur VIP-Lounge im Zielbereich am Sonntag, den 25. September 2016 sollte m�glichst mit der S- und U-Bahn erfolgen, da durch den Streckenverlauf eine ungehinderte Zufahrt zur Stra�e des 17. Juni nicht gegeben ist. Ausgehend vom Brandenburger Tor befindet sich die VIP-Lounge auf der ');
		$this->SetFont('Arial','B',11);
		$this->write(5, 'linken (s�dlichen) Fahrbahnseite ');
		$this->SetFont('Arial','',11);
		$this->write(5, 'der Stra�e des 17. Juni und ist ');
		$this->SetFont('Arial','BU',11);
		$this->write(5, 'nur von dieser Seite zu erreichen.');
	}
	
	private function anfahrtOeffis() {
		$this->SetFont('Arial','B',11);
		$this->write(5, 'Anfahrt mit den �ffentlichen Verkehrsmitteln');
		$this->ln();
		$this->SetFont('Arial','',11);
		$this->write(5, 'Die S-Bahnlinien S1, S2 und S25 sind als Anreisem�glichkeiten zum VIP-Brunch zu empfehlen. Fahren Sie bitte bis zur Station Brandenburger Tor, von dort aus sind es ca. 5 Minuten Fu�weg in Richtung Stra�e des 17. Juni bis auf H�he des Sowjetischen Ehrenmals. Oder Sie nutzen die S- und U-Bahnlinien zum Hauptbahnhof und von dort die U-Bahnlinie 55 zum Brandenburger Tor. Des Weiteren k�nnen Sie auch die S- und U-Bahnlinien zum Potsdamer Platz nutzen. Von dort sind es ca. 10 � 15 Minuten Fu�weg zur VIP-Lounge. ');
		$this->SetFont('Arial','B',11);
		$this->write(5, 'Bitte planen Sie aufgrund der hohen Zuschauer- und Teilnehmerzahlen eventuell mehr Zeit f�r den Weg und den Einlass ein. ');
		$this->SetFont('Arial','',11);
		$this->write(5, 'Wir bitten Sie zu ber�cksichtigen, dass aufgrund der Veranstaltung ');
		$this->SetFont('Arial','B',11);
		$this->write(5, 'keine Busverbindungen ');
		$this->SetFont('Arial','',11);
		$this->write(5, 'zur Stra�e des 17. Juni  bestehen.');
	}
	
	private function zugangViplounge() {
		$this->SetFont('Arial','B',11);
		$this->write(5, 'Zugang zur VIP-Lounge / Sicherheitskontrollen /Absperrungen');
		$this->ln();
		$this->SetFont('Arial','',11);
		$this->write(5, 'Aufgrund h�herer Sicherheitsauflagen ist der Start-/Zielbereich weitr�umig abgesperrt. 
Der Zugang zur VIP-Lounge erfolgt am besten �ber die ');
		$this->SetFont('Arial','BU',11);
		$this->write(5, 's�dliche Ebertstra�e.');
		$this->SetFont('Arial','',11);
		$this->write(5, ' Dort wird es auf H�he der Botschaft der Vereinigten Staaten eine Sicherheitskontrolle (Taschenkontrolle) geben. Bitte achten Sie darauf keine gef�hrlichen Gegenst�nde (wie Messer etc.) und keine Getr�nke �ber 0,5 l mit sich zu f�hren.');
	}
	
	function initText($anrede='Herr', $name='Mustermann', $guests=false) {
		$this->SetLeftMargin(20);
		$this->SetRightMargin(20);
		$betreff='Anmeldebest�tigung zum VIP-Brunch am 25.09.2016';
		$this->addBetreff($betreff);
		$this->addAnrede($anrede, $name);
		$this->addDanke($guests);
		$this->addInhalt();
	}
	
	function ReOutput($dest='', $name='', $isUTF8=false)
{
	// Output PDF to some destination
	$this->Close();
	if(strlen($name)==1 && strlen($dest)!=1)
	{
		// Fix parameter order
		$tmp = $dest;
		$dest = $name;
		$name = $tmp;
	}
	if($dest=='')
		$dest = 'I';
	if($name=='')
		$name = 'doc.pdf';
	switch(strtoupper($dest))
	{
		case 'I':
			// Send to standard output
			if(PHP_SAPI!='cli')
			{
				// We send to a browser
				header('Content-Type: application/pdf');
				header('Content-Disposition: inline; '.$this->_httpencode('filename',$name,$isUTF8));
				header('Cache-Control: private, max-age=0, must-revalidate');
				header('Pragma: public');
			}
			echo $this->buffer;
			break;
		case 'D':
			// Download file
			header('Content-Type: application/x-download');
			header('Content-Disposition: attachment; '.$this->_httpencode('filename',$name,$isUTF8));
			header('Cache-Control: private, max-age=0, must-revalidate');
			header('Pragma: public');
			echo $this->buffer;
			break;
		case 'F':
			// Save to local file
			if(!file_put_contents($name,$this->buffer))
				$this->Error('Unable to create output file: '.$name);
			break;
		case 'S':
			// Return as a string
			return $this->buffer;
		default:
			$this->Error('Incorrect output destination: '.$dest);
	}
	return '';
}
}

// Instanciation of inherited class
//$pdf = new BrunchPDF();
//$pdf->AliasNbPages();
//$pdf->AddPage();
//$pdf->SetFont('Times','',12);
//for($i=1;$i<=20;$i++)
//    $pdf->Cell(0,10,'Printing line number '.$i,0,1);
//$pdf->initText();

//$pdf->Output();


?>