<?php
require('/var/www/html/fpdf/fpdf.php');

class GTGPDFde extends FPDF
{
	// Page header
	function Header()
	{
		// Logo
		$this->Image('../assets/header.jpg',0,0,210);
	}

	// Page footer
	function Footer()
	{
		// Logoleiste
		$this->Image('../assets/logoleiste.jpg',10,240,190);
		$this->Image('../assets/footer.jpg',0,280,210);
	}
	
	function addAnschrift($anrede='Herr', $name='Maximilian Mustermann', $strasse='Musterstr. 123', $plz_ort='12345 Musterstadt') {
		$this->SetY(60);
		$this->SetFont('Arial','',11);
		$this->Cell(50, 6, $anrede);
		$this->ln();
		$this->Cell(50, 6, $name);
		$this->ln();
		$this->Cell(50, 6, $strasse);
		$this->ln(8);
		$this->Cell(50, 6, $plz_ort);
	}
	
	private function addBetreff($text) {
		$this->SetY(50);
		$this->SetFont('Arial','B',10);
		$this->Cell(0, 6, $text);
	}
	
	function addAnrede($nr, $vorname, $name, $gast) {
		$anhang = '';
		if ($gast) {
			$anhang = ' + 1';
		}
		$text = $vorname.' '.$name.' '.$anhang;
		$this->SetY(58);
		$this->SetFont('Arial','B',12);
		$this->Cell(25, 6, $nr);
		$this->Cell(0, 6, $text);
	}

	private function addInhalt() {
		$this->SetY(66);
		$this->SetFont('Arial','BU',10);
		$this->Write(5, 'Bitte bringen Sie diese Anmeldebest�tigung zur Einlasskontrolle mit.');
		$this->Image('../assets/ewerk.png',21,75,20);
		$this->ln(18);
		$this->SetFont('Arial','',9);
		$this->Write(4, 'Wilhelmstr. 43 (gegen�ber dem Finanzministerium)');
		$this->ln();
		$this->Write(4, '10117 Berlin');
		$this->ln(10);
		$this->Write(5, 'DRESSCODE: Cocktail');
		$this->ln(5);
		$this->programmAblauf();
		$this->ln(5);
		$this->anfahrtOeffis();
		$this->ln(5);
		$this->Image('../assets/gettogether.jpg',20,128,170);
	}
	
	private function programmAblauf() {
		$breite = 22;
		$abstand = 118;
		$ln = 7;
		$this->SetFont('Arial','',9);
		$this->SetY(76);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '19:00 Uhr');
		$this->Write(5, 'Einlass');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '19:30 Uhr');
		$this->Write(5, 'Beginn - Sektempfang');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '19:45 Uhr');
		$this->Write(5, 'Offizielle Begr��ung');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, 'ab 21:00 Uhr');
		$this->Write(5, 'Livemusik');
		$this->ln($ln);
	}
	
	private function anfahrtOeffis() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'Wir empfehlen die Anfahrt mit den �ffentlichen Verkehrsmitteln');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, '- Mit der U-Bahn Linie U2 bis Potsdamer Platz oder Mohrenstra�e');
		$this->ln();
		$this->write(4, '- Mit der U-Bahn Line U6 bis Stadtmitte oder Kochstra�e'); 
		$this->ln();
		$this->write(4, '- Oder mit den S-Bahn Linien S1, S2 oder S25 bis Potsdamer Platz'); 
	}
	
	function initText($nr='V-BR-000', $vorname='Max', $name='Mustermann', $guests=false) {
		$this->SetLeftMargin(20);
		$this->SetRightMargin(20);
		$betreff='Anmeldebest�tigung zur GET-TOGETHER PARTY am 23.09.2016:';
		$this->addBetreff($betreff);
		$this->addAnrede($nr, utf8_decode($vorname), utf8_decode($name), $guests);
		$this->addInhalt();
	}
	
}

class GTGPDFen extends FPDF
{
	// Page header
	function Header()
	{
		$this->Image('../assets/header.jpg',0,0,210);
	}

	// Page footer
	function Footer()
	{
		$this->Image('../assets/logoleiste.jpg',10,240,190);
		$this->Image('../assets/footer.jpg',0,280,210);
	}
	
	function addAnschrift($anrede='Herr', $name='Maximilian Mustermann', $strasse='Musterstr. 123', $plz_ort='12345 Musterstadt') {
		$this->SetY(60);
		$this->SetFont('Arial','',11);
		$this->Cell(50, 6, $anrede);
		$this->ln();
		$this->Cell(50, 6, $name);
		$this->ln();
		$this->Cell(50, 6, $strasse);
		$this->ln(8);
		$this->Cell(50, 6, $plz_ort);
	}
	
	private function addBetreff($text) {
		$this->SetY(50);
		$this->SetFont('Arial','B',10);
		$this->Cell(0, 6, $text);
	}
	
	function addAnrede($nr, $vorname, $name, $gast) {
		$anhang = '';
		if ($gast) {
			$anhang = ' + 1';
		}
		$text = $vorname.' '.$name.' '.$anhang;
		$this->SetY(58);
		$this->SetFont('Arial','B',12);
		$this->Cell(25, 6, $nr);
		$this->Cell(0, 6, $text);
	}

	private function addInhalt() {
		$this->SetY(66);
		$this->SetFont('Arial','BU',10);
		$this->Write(5, 'Please bring this registration confirmation with you to enter.');
		$this->Image('../assets/ewerk.png',21,75,20);
		$this->ln(18);
		$this->SetFont('Arial','',9);
		$this->Write(4, 'Wilhelmstr. 43 (across from the Ministery of Finance)');
		$this->ln();
		$this->Write(4, '10117 Berlin');
		$this->ln(10);
		$this->Write(5, 'DRESSCODE: Cocktail');
		$this->ln(5);
		$this->programmAblauf();
		$this->ln(8);
		$this->anfahrtOeffis();
		$this->ln(8);
		$this->Image('../assets/gettogether.jpg',20,128,170);
	}
	
	private function programmAblauf() {
		$breite = 22;
		$abstand = 118;
		$ln = 7;
		$this->SetFont('Arial','',9);
		$this->SetY(76);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '7 pm');
		$this->Write(5, 'Doors open');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '7:30 pm');
		$this->Write(5, 'Begin - Champagne reception');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, '7:45 pm');
		$this->Write(5, 'Official Welcome');
		$this->ln($ln);
		$this->SetX($abstand);
		$this->Cell($breite, 5, 'from 9 pm');
		$this->Write(5, 'Live music');
	}
	
	private function anfahrtOeffis() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'We recommend arriving with public transportation');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, '- With subway U-Bahn line U2 to Potsdamer Platz or Mohrenstrasse');
		$this->ln();
		$this->write(4, '- With subway U-Bahn line U6 to Stadtmitte');		
		$this->ln();
		$this->write(4, '- Or with elevated train S-Bahn lines S1, S2 or S25 to Potsdamer Platz');				
	}
	
	function initText($nr='S-GTG-000', $vorname='Max', $name='Mustermann', $guests=false) {
		$this->SetLeftMargin(20);
		$this->SetRightMargin(20);
		$betreff='Registration confirmation for the GET-TOGETHER PARTY on September 23, 2016:';
		$this->addBetreff($betreff);
		$this->addAnrede($nr, utf8_decode($vorname), utf8_decode($name), $guests);
		$this->addInhalt();
	}
	
}

// Instanciation of inherited class

/*
$pdf = new GTGPDFde();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->initText('S-GTG-000', 'Marcus', 'Mahlo', FALSE);

$pdf->Output();

*/
?>