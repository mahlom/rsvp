<?php
require('/var/www/html/fpdf/fpdf.php');

class BrunchPDF extends FPDF
{
	// Page header
	function Header()
	{
		$this->Image('/var/www/html/assets/header.jpg',0,0,210);
	}

	// Page footer
	function Footer()
	{
//		$this->Image('../assets/logoleiste.jpg',10,240,190);
		$this->Image('/var/www/html/assets/footer.jpg',0,263,210);
	}
}

class BrunchPDFde extends BrunchPDF
{
	private function addBetreff($text) {
		$this->SetY(45);
		$this->SetFont('Arial','B',10);
		$this->Cell(0, 6, $text);
	}
	
	function addAnrede($nr, $vorname, $name, $gast) {
		$anhang = '';
		if ($gast) {
			$anhang = '+ 1';
		}
		$text = $vorname.' '.$name.' '.$anhang;
		$this->SetY(53);
		$this->SetFont('Arial','B',12);
		$this->Cell(25, 6, $nr);
		$this->Cell(0, 6, $text);
	}

	private function addInhalt() {
		$this->SetY(61);
		$this->SetFont('Arial','BU',10);
		$this->Write(5, 'Bitte bringen Sie diese Anmeldebest�tigung zur Einlasskontrolle mit.');
		$this->ln(10);
		$this->SetFont('Arial','',9);
		$this->Write(4, 'Cafe Moskau');
		$this->ln();
		$this->Write(4, 'Karl-Marx-Allee 34');
		$this->ln();
		$this->Write(4, '10178 Berlin');
		$this->ln(10);
		$this->programmAblauf();
		$this->ln(8);
		$this->AnfahrtsHinweise();
		$this->ln(8);
		$this->zugangViplounge();
		$this->ln(8);
		$this->Image('/var/www/html/assets/plan.jpg',25,168,160);
	}
	
	private function programmAblauf() {
		$this->SetFont('Arial','B',9);
		$this->Write(6, 'VIP-Brunch von 10:00 bis 15:00 Uhr');
		$this->ln();
		$this->Write(4, 'Start: Karl-Marx-Allee nahe Otto-Braun-Str.');
		$this->ln();
		$this->Write(6, 'Startzeiten:');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->Write(4, '09:30 Uhr Inlineskating');
		$this->ln();
		$this->Write(4, '10:05 Uhr L�ufer');
	}
	
	private function anfahrtsHinweise() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'Anfahrtshinweise');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, 'Da aufgrund des Laufes mit Verkehrsbehinderungen zu rechnen ist, empfehlen wir eine rechtzeitige Anreise mit der S-Bahn oder der U-Bahn �ber die Bahnh�fe Alexanderplatz oder Strausberger Platz.');
		$this->SetFont('Arial','B',9);
		$this->write(4, ' Der U-Bahnhof Schillingstra�e ist gesperrt.');
	}
	
	private function zugangViplounge() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'Zugang zum Cafe Moskau / Sicherheitskontrollen / Absperrungen:');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, 'Aufgrund von Sicherheitsauflagen ist der Start-/Zielbereich weitr�umig abgesperrt. Der Zugang zum Cafe Moskau erfolgt am besten �ber die ');
		$this->SetFont('Arial','U',9);
		$this->write(4, 'Karl-Marx-Allee aus Richtung Alexanderplatz oder Strausberger Platz, jeweils �ber die s�dliche Seite.');
		$this->SetFont('Arial','',9);
		$this->write(4, ' An allen Zug�ngen zum abgesperrten Bereich wird es  Sicherheitskontrollen (Taschenkontrolle) geben.  Bitte achten Sie darauf, keine gef�hrlichen Gegenst�nde (Messer etc.) und keine Getr�nke �ber 0,5 l mit sich zu f�hren.');
	}
	
	function initText($nr='V-BR-000', $vorname='Max', $name='Mustermann', $guests=false) {
		$this->SetLeftMargin(20);
		$this->SetRightMargin(20);
		$betreff='Anmeldebest�tigung zum VIP-Brunch am 02.04.2017:';
		$this->addBetreff($betreff);
		$this->addAnrede($nr, utf8_decode($vorname), utf8_decode($name), $guests);
		$this->addInhalt();
	}
	
}

class BrunchPDFen extends BrunchPDF
{
	private function addBetreff($text) {
		$this->SetY(45);
		$this->SetFont('Arial','B',10);
		$this->Cell(0, 6, $text);
	}
	
	function addAnrede($nr, $vorname, $name, $gast) {
		$anhang = '';
		if ($gast) {
			$anhang = ' + 1';
		}
		$text = $vorname.' '.$name.' '.$anhang;
		$this->SetY(53);
		$this->SetFont('Arial','B',12);
		$this->Cell(25, 6, $nr);
		$this->Cell(0, 6, $text);
	}

	private function addInhalt() {
		$this->SetY(61);
		$this->SetFont('Arial','BU',10);
		$this->Write(5, 'Please bring this registration confirmation with you to enter.');
		$this->ln(10);
		$this->SetFont('Arial','',9);
		$this->Write(4, 'Cafe Moskau');
		$this->ln();
		$this->Write(4, 'Karl-Marx-Allee 34');
		$this->ln();
		$this->Write(4, '10178 Berlin');
		$this->ln(5);
		$this->programmAblauf();
		$this->ln(8);
		$this->AnfahrtsHinweise();
		$this->ln(8);
		$this->zugangViplounge();
		$this->ln(8);
		$this->Image('/var/www/html/assets/plan.jpg',25,165,160);
	}
	
	private function programmAblauf() {
		$this->SetFont('Arial','B',9);
		$this->Write(6, 'VIP-Brunch from 10 AM until 3 PM');
		$this->ln();
		$this->Write(4, 'Starting line: Karl-Marx-Allee near Otto-Braun-Str.');
		$this->ln();
		$this->Write(6, 'Starting times:');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->Write(4, '9:30 AM inlineskating');
		$this->ln();
		$this->Write(4, '10:05 PM runners');
	}
	
	private function anfahrtsHinweise() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'Getting there');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, 'Due to the ongoing event, there will be road closures and delays in traffic. We recommend using the elevated train (S-Bahn) or subway (U-Bahn) via the stops Alexanderplatz or Strausberger Platz. ');
		$this->SetFont('Arial','B',9);
		$this->write(4, 'The subway station Schillingstra�e is closed.');
	}
	
	private function zugangViplounge() {
		$this->SetFont('Arial','B',9);
		$this->write(6, 'Access to the VIP Lounge / Security controls / Closures');
		$this->ln();
		$this->SetFont('Arial','',9);
		$this->write(4, 'Due to security measures, the start/finish area will be fenced off. The best way to access the Cafe Moskau is ');
		$this->SetFont('Arial','U',9);
		$this->write(4, 'via Karl-Marx-Allee from either Alexanderplatz or Strausberger Platz, on the southern side of the street.');
		$this->SetFont('Arial','',9);
		$this->write(4, ' There will be security / bag control at every entry point to the start/finish area. Be sure not to have any dangerous objects (such as knives, etc.) and no drinks over 0.5 l with you.');
	}
	
	function initText($nr='V-BR-000', $vorname='Max', $name='Mustermann', $guests=false) {
		$this->SetLeftMargin(20);
		$this->SetRightMargin(20);
		$betreff='Registration confirmation for the VIP Brunch on April 2, 2017:';
		$this->addBetreff($betreff);
		$this->addAnrede($nr, utf8_decode($vorname), utf8_decode($name), $guests);
		$this->addInhalt();
	}
	
}

// Instanciation of inherited class
/*
$pdf = new BrunchPDFde();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->initText('V-BR-000', 'Marcus', 'Mahlo', TRUE);

$pdf->Output();

*/
?>